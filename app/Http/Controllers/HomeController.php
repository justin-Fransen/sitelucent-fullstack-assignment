<?php

namespace App\Http\Controllers;

use App\CurrencyRate;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getHome()
    {
        $currencyRates = CurrencyRate::all();
        return view('home.index', compact('currencyRates'));
    }

    public function postConvert(Request $request)
    {
        $amount = $request->get('amount');
        $currency_from = $request->get('currency_from');

        $currencyRate = CurrencyRate::where('name', $currency_from)->first();
        $jsonRates = json_decode($currencyRate->json_rates);
//        dd($jsonRates);

        return view('home.results', compact('amount', 'jsonRates'));
    }
}
