<?php

namespace App\Http\Middleware;

use App\VerifiedIp;
use Closure;
use Illuminate\Support\Facades\Log;

class VerifyIpAddress
{
    /**
     * Show an 403 unauthorized page if IP address is not verified.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = $this->getUserIP();
        if (VerifiedIp::where('ip', $ip)->count() < 1) {
            Log::warning("403 - Unauthorized request, IP not allowed. [$ip]");
            abort(403, 'Unauthorized IP address.');
        }

        return $next($request);
    }

    private function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }
}
