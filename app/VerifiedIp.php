<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifiedIp extends Model
{
    protected $fillable = ['ip'];
}
