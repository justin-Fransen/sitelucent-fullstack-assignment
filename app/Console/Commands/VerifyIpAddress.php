<?php

namespace App\Console\Commands;

use App\VerifiedIp;
use Illuminate\Console\Command;

class VerifyIpAddress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify:ip {ip}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allow an IP address.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ip = $this->argument('ip');
        VerifiedIp::updateOrCreate([
            'ip' => $ip
        ], [
            'ip' => $ip
        ]);
    }
}
