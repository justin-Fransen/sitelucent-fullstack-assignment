<?php

namespace App\Console\Commands;

use App\CurrencyRate;
use Illuminate\Console\Command;

class ImportLatestMoneyRates extends Command
{
    protected $signature = 'import:money_rates';

    protected $description = 'Crawl JSON currency rate data and insert them into the database.';

    public function __construct()
    {
        parent::__construct();
    }

    private function getCurrencyNameFromUrl($url) {
        $pattern = '/daily\/(.*)\.json/';
        preg_match($pattern, $url, $matches);
        return $matches[1];
    }

    private function startCrawler() {
        // Get the links to the json objects
        $url = 'http://www.floatrates.com/json-feeds.html';
        $contents = file_get_contents($url);
        $pattern = '/<li><a href="(.*)"[\b>](.*) JSON Feed<\/a><\/li>/i';
        preg_match_all($pattern, $contents, $matches);

        // Loop through the json URLs
        foreach($matches[1] as $key => $jsonUrl) {
            try { // Catch file_get_contents exceptions
                $currencyTitle = $matches[2][$key];
                $currencyName = $this->getCurrencyNameFromUrl($jsonUrl);
                $json = file_get_contents($jsonUrl);

                // Create or update CurrencyRate
                CurrencyRate::updateOrCreate([
                    'name' => $currencyName
                ],[
                    'title' => $currencyTitle,
                    'name' => $currencyName,
                    'json_url' => $jsonUrl,
                    'json_rates' => $json
                ]);
            } catch(\Exception $e) {
                echo "$e\n";
            }
        }
    }

    public function handle()
    {
        $this->startCrawler();
        return 1;
    }
}
