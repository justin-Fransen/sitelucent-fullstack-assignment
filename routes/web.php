<?php

Route::group(['middleware' => 'verify.ip'], function() {
    Route::get('login', 'AuthController@getLogin')->name('login')->middleware('guest');
    Route::post('login', 'AuthController@postLogin');
    Route::get('/', 'HomeController@getHome')->name('home');
    Route::post('convert', 'HomeController@postConvert')->name('convert');
    Route::get('logout', 'AuthController@getLogout')->name('logout');
});

//Route::get('test', function() {
//    $rates = \Illuminate\Support\Facades\Artisan::call('import:money_rates');
//    dd($rates);
//});