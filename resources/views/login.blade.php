<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SLFSA login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    {{--A little ugly to add uncompressed CSS--}}
    <link rel="stylesheet" href="/css/login.css"/>
    <link rel="stylesheet" href="/css/bootstrap-dirty-override.css" />
</head>
<body>
<div class="center-wrapper">
    <div class="centered">
        <a href="/">
        <img src="https://www.sitelucent.com/img/logo/01_LOGO_DEF_White.png" alt="SiteLucent logo" class="logo"/>
        </a>

        <div class="login-box">
            @if(session('errors'))
                <div class="error-message">
                    {{ session('errors')->first('error') }}
                </div>
            @endif
            <form action="/login" method="post" accept-charset="utf-8">
                @csrf
                <input type="text" name="email" value="" placeholder="Email" class="form-control"/>
                <input type="password" name="password" value="" placeholder="Password" class="form-control"/>

                <label class="form-check-label" for="defaultCheck1">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" />
                    Remember me
                </label>

                <input type="submit" name="submit" value="Login" class="btn btn-primary btn-block btn-flat" />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>