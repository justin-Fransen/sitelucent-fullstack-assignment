@extends('layout')

@section('content')
    <h1>Money rates</h1>
    <div class="content-card">
        <table>
            <thead>
                <tr>
                    <td>Currency</td>
                    <td>Amount</td>
                </tr>
            </thead>
            <tbody>
            @foreach($jsonRates as $jsonRate)
                <tr>
                    <td>{{ $jsonRate->name }} ({{ $jsonRate->code }})</td>
                    <td>{{ $jsonRate->rate*$amount }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection