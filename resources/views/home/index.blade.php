@extends('layout')

@section('content')
    <h1>Money rates checkup</h1>
    <div class="content-card">
        <form action="/convert" method="post">
            @csrf
            <div class="form-group">
                <label for="amount">Currency from amount</label>
                <input type="number" class="form-control" id="amount" placeholder="Please enter an amount..."
                       name="amount"/>
            </div>
            <div class="form-group">
                <label for="currencyFrom">Currency from</label>
                <select class="form-control" id="currencyFrom" name="currency_from">
                    @foreach($currencyRates as $currencyRate)
                        <option value="{{ $currencyRate->name }}">{{ $currencyRate->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="text-right">
                <input type="submit" name="submit" value="Convert" class="btn btn-primary btn-flat"/>
            </div>
        </form>
    </div>
@endsection