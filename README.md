## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)



## About The Project

This project is created as an assignment project for SiteLucent.

### Built With

* [Laravel](https://laravel.com)



## Getting Started

To get a local copy up and running and follow these simple steps.

### Prerequisites

* [Composer](https://getcomposer.org/)
* [Laravel Homestead](https://laravel.com/docs/homestead)

### Installation

This is an installation procedure special to the Homestead development environment.
 
1. Clone the repo
```sh
git clone https://justin-Fransen@bitbucket.org/justin-Fransen/sitelucent-fullstack-assignment.git
```
2. Add the project to homestead.yaml
3. add the map domain to the hosts file
4. Boot homestead & **provision**
```sh
cd path/to/homestead/folder
vagrant up
vagrant provision
```
5. SSH into homestead (use command below or use SSH client with username: vagrant, password: vagrant)
```sh
vagrant ssh
```
6. Install packages using composer
```sh
composer install
```
7. Copy the .env.example file to .env and set environment settings
8. Generate a project key (result in .env file)
```sh
php artisan key:generate
```



## Usage

There might be some difficulties, since you cannot register for an account in the UI.

### Migrate the database

Migrate the database to get the project up and running.
```sh
php artisan migrate
php artisan db:seed
```

### Register a new account

1. Open tinker
```sh
php artisan tinker
```
2. Create a new User model
```sh
\App\User::create([
    'email' => 'yourmail@domain.ext',
    'password' => 'your_password'
]);
```

### Verify an IP address

```
php artisan verify:ip x.x.x.x
```